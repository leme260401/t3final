package com.example.t3final.recycler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.t3final.DetalleActivity;
import com.example.t3final.MapsActivity;
import com.example.t3final.PokemonActivity;
import com.example.t3final.R;
import com.example.t3final.pokemons.Pokemons;
import com.google.gson.Gson;

import java.util.List;

public class AdapterPokemon extends RecyclerView.Adapter<AdapterPokemon.PokemonViewHolder>{

    private List<Pokemons> listaPokemons;
    private TextView nombre;
    private TextView tipo;
    private ImageView imagen;
    public String urlImage;
    private Button btnVer;

    public AdapterPokemon(List<Pokemons> listaPokemons) {
        this.listaPokemons = listaPokemons;
    }

    @NonNull
    @Override
    public AdapterPokemon.PokemonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pokemon, parent, false);
        PokemonViewHolder vh = new PokemonViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPokemon.PokemonViewHolder holder, int position) {
        nombre = holder.itemView.findViewById(R.id.tvPokemons);
        tipo = holder.itemView.findViewById(R.id.tvTipo);
        imagen = holder.itemView.findViewById(R.id.igPokemon);
        Pokemons pokemon = listaPokemons.get(position);
        nombre.setText(pokemon.getNombre());
        urlImage = pokemon.getUrl_imagen();
        tipo.setText(pokemon.getTipo());
        Glide.with(holder.itemView.getContext()).load(urlImage).into(imagen);

        btnVer = holder.itemView.findViewById(R.id.btnVer);

        btnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), DetalleActivity.class);
                intent.putExtra("Pokemon", new Gson().toJson(pokemon));
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaPokemons.size();
    }

    // Por cada clase adapter, se necesita una clase ViewHolder
    public class PokemonViewHolder extends RecyclerView.ViewHolder {

        public PokemonViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}