package com.example.t3final;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.t3final.pokemons.Pokemons;
import com.example.t3final.recycler.AdapterPokemon;
import com.example.t3final.services.Servicio;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PokemonActivity extends AppCompatActivity {

    private androidx.recyclerview.widget.RecyclerView recyclerView;
    private ImageView imageView;
    private Context context;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon);

        recyclerView = findViewById(R.id.rvPokemons);
        imageView = findViewById(R.id.igPokemon);
        textView = findViewById(R.id.tvPokemons);
        textView = findViewById(R.id.tvTipo);
        context = this.getApplicationContext();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Servicio service = retrofit.create(Servicio.class);
        Call<List<Pokemons>> call = service.getByQuery("abc");

        call.enqueue(new Callback<List<Pokemons>>() {
            @Override
            public void onResponse(Call<List<Pokemons>> call, Response<List<Pokemons>> response) {
                if (response.code()==200){
                    List<Pokemons> pokemons = response.body();

                    Log.i("MY_APP" , "Corrio exitosamente");
                    Log.i("MY_APP" , new Gson().toJson(pokemons));
                    //Envia los datos al adaptador
                    recyclerView.setAdapter(new AdapterPokemon(pokemons));
                }else {
                    Log.i("MY_APP" , "Intentalo de nuevo, algo falló");
                }
            }

            @Override
            public void onFailure(Call<List<Pokemons>> call, Throwable t) {
                Log.i("MY_APP" , "No pudimos conectarnos con la aplicación");
            }
        });
    }
}