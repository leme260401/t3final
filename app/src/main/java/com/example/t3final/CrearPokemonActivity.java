package com.example.t3final;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.t3final.pokemons.Pokemons;
import com.example.t3final.services.Servicio;

import java.io.ByteArrayOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CrearPokemonActivity extends AppCompatActivity {

    private Button btnEnviar;
    private EditText etName;
    private EditText etType;
    private EditText etUrl;
    private EditText etLatitude;
    private EditText etLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_pokemon);

        etName = findViewById(R.id.edNombre);
        etType = findViewById(R.id.edTipo);
        etUrl = findViewById(R.id.edUrl);
        etLatitude = findViewById(R.id.edLatitude);
        etLongitude = findViewById(R.id.edLongitude);
        btnEnviar = findViewById(R.id.btnCrear);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Servicio service = retrofit.create(Servicio.class);


        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pokemons pokemon = new Pokemons();
                pokemon.setNombre(etName.getText().toString());
                pokemon.setTipo(etType.getText().toString());
                pokemon.setUrl_imagen(etUrl.getText().toString());
                pokemon.setLatitude(Float.parseFloat(etLatitude.getText().toString()) * -1);
                pokemon.setLongitude(Float.parseFloat(etLongitude.getText().toString()) * -1);

                Call<Pokemons> call = service.create(pokemon);

                call.enqueue(new Callback<Pokemons>() {
                    @Override
                    public void onResponse(Call<Pokemons> call, Response<Pokemons> response) {
                        Intent intent = new Intent(CrearPokemonActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                    @Override
                    public void onFailure(Call<Pokemons> call, Throwable t) {
                    }
                });
            }
        });
    }


}