package com.example.t3final.services;

import com.example.t3final.pokemons.Pokemons;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Servicio {
    @GET("pokemons/N000170218/")
    Call<List<Pokemons>> getALL();

    @GET("pokemons/N000170218/")
    Call<List<Pokemons>> getByQuery(@Query("query") String query);

    @POST("pokemons/N000170218/crear")
    Call<Pokemons> create(@Body Pokemons pokemon);
}
